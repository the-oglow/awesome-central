# ASS - Awesome Snap Shots

Snee-Snaa-Snappee Shots Repository

## Powered by

<a href="https://de.wikipedia.org/wiki/RTL_Samstag_Nacht#Karl_Ranseier" target="_blank" title="Carl Ranseier, the least successful developer of the outgoing 19th century has died.">
<img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/10107405/karl-ranseier-logo.jpg" alt="Karl Ranseier" height="75px" />
<br><sup><strong>Karl Ranseier</strong></sup></a>

## Author

[Oliver Glowa](https://gitlab.com/ollily)

## License

This project is released under version 2.0 of the [Apache License](https://gitlab.com/the-oglow/global-pom/-/raw/master/LICENSE).  
Please see the [license file](https://gitlab.com/the-oglow/global-pom/-/raw/master/LICENSE) for more information.

***
<sup>(c) 2020 by [Oliver Glowa](https://gitlab.com/ollily) </sup>
